<?php

use App\Models\Session;
use App\Models\SessionMember;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::transaction(function () {
            $group_double_sessions = Session::query()
                ->select(
                    DB::raw('GROUP_CONCAT(id) as id'),
                    DB::raw('GROUP_CONCAT(id) as ids'),
                    'start_time',
                    'session_configuration_id'
                )
                ->groupBy('start_time', 'session_configuration_id')
                ->havingRaw('count(*) > 1')
                ->orderByDesc('id')
                ->get();

            foreach ($group_double_sessions as $session) {
                $doubles_sessions_ids = array_diff(explode(',', $session->ids), [$session->id]);
                $sessions_members = SessionMember::query()
                    ->whereIn('session_id', $doubles_sessions_ids)
                    ->get();

                foreach ($sessions_members as $session_member) {
                    if (
                        $session_member->where('client_id', $session_member->client_id)
                        ->where('session_id', $session->id)
                        ->get()
                        ->isEmpty()
                    ){
                        $session_member->session_id = $session->id;
                        $session_member->save();
                    }else{
                        $session_member->delete();
                    }
                }
                Session::destroy($doubles_sessions_ids);
            }
        });

        Schema::table('sessions', function (Blueprint $table) {
            $table->unique(['start_time', 'session_configuration_id']);
        });

        Schema::table('session_members', function (Blueprint $table) {
            $table->unique(['session_id', 'client_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
